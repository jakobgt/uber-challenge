# This file is used to test the performance of varies strategies for fetching stops from the
# NextBus server.

{StopFetcher} = require 'src/stop-fetcher'
{ParallelStopFetcher} = require 'src/stop-fetcher'
{StopsQuerier} = require 'src/stops-querier'
{CachedStopFetcher} = require 'src/stop-fetcher'

fetcher = new ParallelStopFetcher() #new ParallelStopFetcher()
fetcher.getStops (result) ->
  # The following command together with a unix pipe can be used for updating the json cache:
  # console.log JSON.stringify(result)
  console.log "Successfully retrieved #{result.length} stops."
  querier = new StopsQuerier(result)
  console.log "initialized the querier"
#  for i in [0..100000]
#    querier.find i




