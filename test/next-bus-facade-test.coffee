{NextBusFacade} = require '../src/next-bus-facade'
{NextBusSource} = require '../src/next-bus-facade'
{FileDataSource} = require './file-data-source'
should = require 'should'
fs = require 'fs'

describe 'NextBusFacade', ->
  facade = null

  beforeEach (done) ->
    facade = new NextBusFacade(new FileDataSource())
    facade.onError (msg, err) ->
      throw new Error (msg + err)
    done()

  # TODO(jakob): Add tests for _extractAgencies.
  describe 'getAgencies', ->
    it 'calls the callback', (done) ->
      facade.getAgencies ->
        done()
    it 'retrieves a list of agencies', (done) ->
      facade.getAgencies (result) ->
        result.length.should.eql 3
        done()
    it 'first agency is AC Transit', (done) ->
      facade.getAgencies (result) ->
        result[0].title.should.eql 'AC Transit'
        done()

  describe 'getRoutesForAgency', ->
    it 'calls the callback', (done) ->
      facade.getRoutesForAgency 'DART', ->
        done()
    it 'retrieves the routes', (done) ->
      facade.getRoutesForAgency 'DART', (result) ->
        result.length.should.eql 3
        done()
    it 'retrieves the Daly City route', (done) ->
      facade.getRoutesForAgency 'DART', (result) ->
        result[0].title.should.eql 'F-Market & Wharves'
        done()

  describe 'getStopsForRoute', ->
    it 'calls the callback', (done) ->
      facade.getStopsForRoute 'sf-muni', 'n', ->
        done()

  describe 'getDeparturesForStop', ->
    it 'calls the callback', (done) ->
      facade.getStopsForRoute 'sf-muni', 'n', ->
        done()
    it 'retrieves four predictions', (done) ->
      facade.getDeparturesForStop 'sf-muni', 1234, (result) ->
        result.length.should.eql 4
        done()
    it 'retrieves first prediction', (done) ->
      facade.getDeparturesForStop 'sf-muni', 1234, (result) ->
        result[0].epochTime.should.eql '1393016243175'
        done()
    it 'sets the routetitle', (done) ->
      facade.getDeparturesForStop 'sf-muni', 1234, (result) ->
        result[0].routeTitle.should.eql 'Powell to BART Station'
        done()

describe 'NextBusSource', ->
  source = new NextBusSource()
  describe 'getAgencies', ->
    it 'calls the callback', (done) ->
      source.getAgencies ->
        done()

  describe 'getRoutesForAgency', ->
    it 'calls the callback', (done) ->
      source.getRoutesForAgency 'DART', ->
        done()

  describe 'getStopsForRoute', ->
    it 'calls the callback', (done) ->
      source.getStopsForRoute 'sf-muni', 'N', ->
        done()

  describe 'getDeparturesForStop', ->
    it 'calls the callback (is flaky, as the server call can timeout.)', (done) ->
      source.getDeparturesForStop 'emery', 5357, ->
        done()