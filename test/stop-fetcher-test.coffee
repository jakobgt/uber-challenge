{NextBusFacade} = require '../src/next-bus-facade.js'
{StopFetcher} = require '../src/stop-fetcher.js'
{ParallelStopFetcher} = require '../src/stop-fetcher.js'
{CachedStopFetcher} = require '../src/stop-fetcher.js'
{FileDataSource} = require './file-data-source.js'
should = require 'should'

describe 'StopFetcher', ->
  fetcher = null
  beforeEach ->
    facade = new NextBusFacade(new FileDataSource())
    # Why does this make it global?
    facade.onError (msg, err) ->
      throw new Error "#{msg}: #{err}"
    #fetcher = new StopFetcher(facade)
    fetcher = new ParallelStopFetcher(facade)

  describe 'getStopsForRoutes', ->
    it 'returns a list', (done) ->
      fetcher.getStopsForRoutes 'sf-muni', [{tag: 'F'}], [], (result) ->
        result.length.should.eql 67
        done()
    it 'the first tag is 5240', (done) ->
      fetcher.getStopsForRoutes 'sf-muni', [{tag: 'F'}], [], (result) ->
        result[0]['tag'].should.eql 5240
        done()
    it 'the last tag is 5239', (done) ->
      fetcher.getStopsForRoutes 'sf-muni', [{tag: 'F'}], [], (result) ->
        result[66]['tag'].should.eql 5239
        done()
    it 'attaches the agency', (done) ->
      fetcher.getStopsForRoutes 'sf-muni', [{tag: 'F'}], [], (result) ->
        result[0]['agency'].should.eql 'sf-muni'
        done()
    it 'attaches an id', (done) ->
      fetcher.getStopsForRoutes 'sf-muni', [{tag: 'F'}], [], (result) ->
        result[0]['id'].should.eql 0
        done()
    it 'increments the id', (done) ->
      fetcher.getStopsForRoutes 'sf-muni', [{tag: 'F'}], [], (result) ->
        result[1]['id'].should.eql 1
        done()

  describe 'getStopsForAgencies', ->
    it 'returns a list for an agency in Northern California', (done) ->
      fetcher.getStopsForAgencies [{tag: 'sf-muni', regionTitle: 'California-Northern'}], [], (result) ->
        # The test data has 3 routes, and for each route 67 stops.
        result.length.should.eql 201
        done()
    it 'the first returned is a stop', (done) ->
      fetcher.getStopsForAgencies [{tag: 'sf-muni', regionTitle: 'California-Northern'}], [], (result) ->
        result[0]['tag'].should.eql 5240
        done()
    it 'the 68 returned is a stop', (done) ->
      fetcher.getStopsForAgencies [{tag: 'sf-muni', regionTitle: 'California-Northern'}], [], (result) ->
        result[67]['tag'].should.eql 5240
        done()
    it 'the 201 returned is a stop', (done) ->
      fetcher.getStopsForAgencies [{tag: 'sf-muni', regionTitle: 'California-Northern'}], [], (result) ->
        result[200]['tag'].should.eql 5239
        result[200]['agency'].should.eql 'sf-muni'
        done()
    it 'id is incremented over routes', (done) ->
      fetcher.getStopsForAgencies [{tag: 'sf-muni', regionTitle: 'California-Northern'}], [], (result) ->
        result[200]['id'].should.eql 200
        done()

    describe 'multiple agencies', ->
      # Sort of a test fixture for this context
      retrieveMultipleStops = (callback) ->
        fetcher.getStopsForAgencies [{tag: 'sf-muni', regionTitle: 'California-Northern'},
          {tag: 'actransit', regionTitle: 'California-Northern'}], [], callback

      it 'returns correct number of stops', (done) ->
        retrieveMultipleStops (result) ->
          # Two agencies, three routes each and 67 stops for each route.
          result.length.should.eql 402
          done()
      it 'first agency is set to sf-muni', (done) ->
        retrieveMultipleStops (result) ->
          # Two agencies, three routes each and 67 stops for each route.
          result[0]['agency'].should.eql 'sf-muni'
          done()
      it 'last agency is set to actransit', (done) ->
        retrieveMultipleStops (result) ->
          # Two agencies, three routes each and 67 stops for each route.
          result[401]['agency'].should.eql 'actransit'
          done()

  describe 'getStop', ->
    it 'returns a list for an agency in Northern California', (done) ->
      fetcher.getStops (result) ->
        # The test data has 3 agencies, of which two are in Northern California and each of those
        # have 67 stops (as duplicate routes are not returned)
        result.length.should.eql 2 * 67
        done()

    it 'the first returned is a stop', (done) ->
      fetcher.getStops (result) ->
        result[0]['tag'].should.eql 5240
        done()
    it 'the 68 returned is a stop', (done) ->
      fetcher.getStops (result) ->
        result[67]['tag'].should.eql 5240
        done()
    it 'the 402 returned is a stop', (done) ->
      fetcher.getStops (result) ->
        result[133]['tag'].should.eql 5239
        done()

describe 'CachedStopFetcher', ->
  filename = 'data/stop-data.json'
  it 'returns 7 stops', (done) ->
    fetcher = new CachedStopFetcher filename
    fetcher.getStops (result) ->
      result.length.should.eql 7
      done()