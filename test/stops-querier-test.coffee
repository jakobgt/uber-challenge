{StopsQuerier} = require 'src/stops-querier.js'
{CachedStopFetcher} = require 'src/stop-fetcher.js'
sinon = require 'sinon'

should = require 'should'

describe 'StopsQuerier', ->
  stops = []
  querier = null
  beforeEach (done) ->
    fetcher = new CachedStopFetcher 'data/stop-data.json'
    fetcher.getStops (result) ->
      stops = result
      querier = new StopsQuerier stops
      done()

  describe 'nearestStops', ->
    it 'returns an empty list', ->
      querier = new StopsQuerier []
      querier.nearestStops(0,0).length.should.eql 0
    it 'returns one neighbor within 100 m', ->
      querier.nearestStops(37.8094, -122.2357599, 0.1).length.should.eql 1
    it 'returns 6 neighbour within 1 km', ->
      querier.nearestStops(37.8094, -122.2357599, 1).length.should.eql 6
    it 'returns 7 neighbour within 100 km', ->
      querier.nearestStops(37.8094, -122.2357599, 100).length.should.eql 7
    it 'returns 33 on the full data set within 0.5 km of a point in San Francisco', (done) ->
      fetcher = new CachedStopFetcher 'data/full-data-set.json'
      fetcher.getStops (result) ->
        stops = result
        querier = new StopsQuerier stops
        querier.nearestStops(37.77140344165701,-122.4185436831055, 0.5).length.should.eql 33
        done()

  describe 'pickPotentialStops', ->
    it 'returns all on very long distance', ->
      querier.pickPotentialStops(0, 0, 1000).length.should.eql 7
    it 'returns one in very close proximity', ->
      #console.log querier.pickPotentialStops(37.8094, -122.2357599, 0.01)
      querier.pickPotentialStops(37.8094, -122.2357599, 0.01).length.should.eql 1

  describe 'find', ->
    it 'returns the one specified by the id', ->
      querier.find(6).tag.should.eql '1014270'
    it 'returns null, when it cannot find the stop', ->
      (querier.find(10) == null).should.be.true
    it 'accepts a string', ->
      querier.find('6').tag.should.eql '1014270'
    it 'returns null, if the string is not an integer', ->
      (querier.find('asdfasdf') == null).should.be.true
    it 'NaN cannot be used as key', ->
      querier.stops.push
        id: NaN
        tag: 'NaNKeyed'
      (querier.find(NaN) == null).should.be.true

  describe 'getDepartures', ->
    it 'forwards empty list, when the stop is not found', (done) ->
      querier.getDepartures 10, (result) ->
        result.length.should.eql 0
        done()
    it 'calls callback with agency and stopId', ->
      departuresForStop = sinon.spy()
      querier.facade.getDeparturesForStop = departuresForStop
      querier.getDepartures 6, () ->
      departuresForStop.calledWithMatch('actransit', '57980').should.be.true
    it 'supports stop objects', ->
      stop = querier.find 6
      departuresForStop = sinon.spy()
      querier.facade.getDeparturesForStop = departuresForStop
      querier.getDepartures stop, () ->
      departuresForStop.calledWithMatch('actransit', '57980').should.be.true
    it 'supports stopId as string', ->
      stop = querier.find '6'
      departuresForStop = sinon.spy()
      querier.facade.getDeparturesForStop = departuresForStop
      querier.getDepartures stop, () ->
      departuresForStop.calledWithMatch('actransit', '57980').should.be.true
