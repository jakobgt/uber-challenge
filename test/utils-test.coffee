{clone} = require 'src/utils'
should = require 'should'

describe 'clone', ->
  it 'clones properties', ->
    srcObj = { property: 'oldValue'}
    newObj = clone srcObj
    srcObj.property = 'newValue'
    newObj.property.should.eql 'oldValue'
  it 'clones shallowly the properties', ->
    srcObj = { property: [1]}
    newObj = clone srcObj
    srcObj.property.push 2
    newObj.property.should.eql [1, 2]