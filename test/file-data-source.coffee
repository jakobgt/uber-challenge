fs = require 'fs'

root = exports ? this

# Test data source.
class FileDataSource
  getAgencies: (resultCallback) =>
    @_fileStream 'nextbus-agencies.xml', resultCallback

  getRoutesForAgency: (agency, resultCallback) =>
    @_fileStream 'nextbus-routes.xml', resultCallback

  getStopsForRoute: (agency, route, resultCallback) =>
    @_fileStream 'nextbus-stops.xml', resultCallback

  getDeparturesForStop: (agency, stopId, resultCallback) =>
    @_fileStream 'predictions.xml', resultCallback

  _fileStream: (filename, callback) =>
    # fs.createReadStream returns a stream that is expected by the resultCallback
    callback(fs.createReadStream this._pathForFile(filename))

  _pathForFile: (filename) ->
    "#{__dirname}/../data/#{filename}"

root.FileDataSource = FileDataSource