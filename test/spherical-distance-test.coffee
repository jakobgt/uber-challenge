sd = require 'lib/spherical-distance.js'

describe 'distance', ->
  it 'returns a distance of zero', ->
    start = {latitude: 1, longitude: 1}
    sd.getDistance(start, start).should.be.approximately(0, 0.1)

  it 'returns a distance of 1', ->
    start = {latitude: 37.8094, longitude: -122.2357599}
    end = {latitude:  37.8094, longitude: -122.2457599}
    sd.getDistance(start, end).should.be.approximately(0.8, 0.1)