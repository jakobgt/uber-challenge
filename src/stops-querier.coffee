# This class is the facade for the HTTP server for querying the Stops.

sd = require '../lib/spherical-distance.js'
{NextBusFacade} = require './next-bus-facade'
{clone} = require './utils'
geohash = require('ngeohash');
root = exports ? this

class StopsQuerier
  # Constructs a stops querier over the given stops.  The stops should be given as a list, where
  # each stop should have at least the following attributes:
  # id     # Unique identifier for the stop
  # lat    # The latitude of the physical location of the stop
  # lon    # The longitude of the physical location of the stop
  constructor: (stops) ->
    @stops = stops
    @facade = new NextBusFacade()
    # We initialize the index and geohash index.
    @index = {}
    @geohash = {}
    for stop in @stops
      @index[stop.id] = stop
      # We initialize the geohashes by putting the points into GeoHash buckets.
      for i in [3..7]
        hash = geohash.encode(stop.lat, stop.lon, i)
        @geohash[hash] ?= []
        @geohash[hash].push stop

  # Given a lat/long pair, this method returns the list of stops that are within distance (in km)
  # of the pair.
  nearestStops: (latitude, longitude, distance = 2) ->
    stops = []
    for stop in @pickPotentialStops(latitude, longitude, distance)
      stopDistance = sd.getDistance({ latitude: latitude, longitude: longitude },
        { latitude: stop.lat, longitude: stop.lon })
      if stopDistance < distance
        # Clone such that we can set the distance.
        newStop = clone stop
        newStop.distance = stopDistance
        stops.push newStop
    return stops

  # Given a lat, long pair, this method returns that potential stops (as given by geohashes), that
  # are within distance of the pair.
  pickPotentialStops: (lat, long, distance) =>
    level = @chooseHashLevel distance
    # If the level is null (the distance is too big), we need to run through all points.
    if !level?
      return @stops
    else
      pointHash = geohash.encode lat, long, level
      # Stops contain the potential stops.
      stops = []
      # We iterate through all directions.
      for i in [-1..1]
        for j in [-1..1]
          hash = geohash.neighbor pointHash, [i,j]
          # Some of these neighbours might not be set.
          if @geohash[hash]?
            stops = stops.concat @geohash[hash]
      return stops

  # Given a distance, this function works out the lowest geohash level (i.e., resolution) that is
  # needed for covering the distance. Distance given is in km.
  chooseHashLevel: (distance) ->
    level = switch
      when distance < 0.076  then 7
      when distance < 0.61 then 6
      when distance < 2.4 then 5
      when distance < 20 then 4
      when distance < 77 then 3
      else null
    return level

  # Returns the stop with the given id. If not present, null is returned.
  find: (id) =>
    if typeof id is 'string'
      id = parseInt id
    return @index[id] ? null

  # Given a stop_id or a stop, returns the departures for that stop. If no departures are present or
  # the stop does not exists, then the empty list is returned.
  getDepartures: (stop, callback) =>
    if typeof stop is 'number' or typeof stop is 'string'
      stop = @find stop
    if stop?
      @facade.getDeparturesForStop stop.agency, stop.stopId, callback
    else
      callback []

root.StopsQuerier = StopsQuerier