root = exports ? this
# Copied from http://jsperf.com/cloning-an-object/2
clone = (obj) ->
  target = {}
  for prop, val of obj
    if obj.hasOwnProperty(prop)
      target[prop] = val
  return target

root.clone = clone
