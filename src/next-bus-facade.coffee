# NextBusFacade is the interface to the NextBus API server.  It utilizes a DataSource (see
# NextBusSource) for retrieving streams of data, that it then fully consumes, sanitizes and
# forwards to the callback.

{parseString} = require 'xml2js'
http = require 'http'

class NextBusFacade
  _dataSource: null

  constructor: (dataSource = new NextBusSource()) ->
    # We initialize the instance variables.
    @_errCallbacks = []
    @_dataSource = dataSource
    @onError (msg, err) =>
      console.log "Error: '#{msg}': #{err}"

  # Calls callback with a list of agencies.
  getAgencies: (callback) =>
    @_getData @_dataSource.getAgencies, @_exractAgencies, callback

  # Returns the list of routes for the given agency. The agency should be the uniquely defining
  # name for that route (ie., tag for a NextBus agency).
  getRoutesForAgency: (agency, callback) ->
    @_getData (responseCallback) =>
      @_dataSource.getRoutesForAgency agency, responseCallback
    , @_extractRoutes, callback

  # Calls the callback with the stops for the given agency and route.
  getStopsForRoute: (agency, route, callback) ->
    @_getData (responseCallback) =>
      @_dataSource.getStopsForRoute agency, route, responseCallback
    , @_extractStops, callback

  # Calls the callback with the departure predictions for the stop and agency
  getDeparturesForStop: (agency, stopId, callback) ->
    @_getData (responseCallback) =>
      @_dataSource.getDeparturesForStop(agency, stopId, responseCallback)
    , @_extractDepartures, callback

  # Adds an error callback. The callback is called if any of the methods encounters an error.
  onError: (callback) ->
    if !(@_errCallbacks?)
      @_errCallbacks = []
    this._errCallbacks.push callback

  error: (msg, error) =>
    for callback in @_errCallbacks
      callback(msg, error)

  # This method calls dataMethod, that takes a callback with readable stream of the data.
  # The resultSanitizer is called on the resulting data, before being feed to callback.
  _getData: (dataMethod, resultSanitizer, callback) =>
    request = dataMethod (stream) =>
      bodyChunks = [];
      stream.on('data', (chunk) ->
        bodyChunks.push chunk
      ).on 'end', =>
        body = Buffer.concat bodyChunks
        parseString body, (err, result) =>
          if err?
            @error "Could not parse XML: ", err
          else if @_hasErrors result
            @error "Error with request: #{JSON.stringify result.body.Error}"
          else
            callback (resultSanitizer result)

    request.on 'error', (e) =>
      this.error 'Could not retrieve data. ', e

  _hasErrors: (xml2jsResult) =>
    xml2jsResult.body? && xml2jsResult.body.Error?
  # Given the NextBus data in XML2Js format, this method weeds the data, such that is it useable.
  _exractAgencies: (result) =>
    if result.body?
      if result.body.agency?
        agencies = []
        for agency in result.body.agency
          agencies.push agency['$']
        return agencies
      else
        this.error "Did not find the expected agency tag inside the body element", null
    else
      this.error "Did not find a root body tag", null
    return []

  # Given the NextBus list of routes for an agency, this method weeds the data, thereby removing
  # the body root and agency tags and removing the '$' tag denoting attributes.
  _extractRoutes: (xml2jsResult) =>
    if xml2jsResult.body?
      if xml2jsResult.body.route?
        routes = []
        for route in xml2jsResult.body.route
          routes.push route['$']
        return routes
      else
        this.error "Did not find the expected route tag inside the body element", null
    else
      this.error "Did not find a root body element", null
    return []

  # Given the result of the NextBus stops for a route API call, this methods extracts the stops
  # from the output.
  _extractStops: (xml2jsResult) =>
    if xml2jsResult.body?
      if xml2jsResult.body.route?
        if xml2jsResult.body.route[0].stop?
          stops = []
          for stop in xml2jsResult.body.route[0].stop
            stops.push stop['$']
          return stops
        else
          this.error "Did not find a stop element inside the route element.", null
      else
        this.error "Did not find a route element inside the body element.", null
    else
      this.error "Did not find a root body element", null
    return []

  _extractDepartures: (xml2jsResult) =>
    if xml2jsResult.body?
      if xml2jsResult.body.predictions?
        routeTitle = xml2jsResult.body.predictions[0]['$']['routeTitle']
        predictions = []
        if xml2jsResult.body.predictions[0]? && xml2jsResult.body.predictions[0].direction?
          for direction in xml2jsResult.body.predictions[0].direction
            routeTitle += " #{direction['$'].title}"
            for prediction in direction.prediction
              prediction = prediction['$']
              prediction['routeTitle'] = routeTitle
              predictions.push prediction
          return predictions
      else
        this.error 'Expected to find predictions elements within the body element, but did not.'
    else
      this.error "Did not find a root body element", null
    return null

root = exports ? this
root.NextBusFacade = NextBusFacade

# NextBusSource is a wrapper for the http get requests to the NextBus API server.
class NextBusSource
  getAgencies: (resultCallback) ->
    http.get "http://webservices.nextbus.com/service/publicXMLFeed?command=agencyList",
      resultCallback

  getRoutesForAgency: (agency, resultCallback) ->
    http.get "http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=#{agency}",
      resultCallback

  getStopsForRoute: (agency, route, resultCallback) ->
    http.get "http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig" +
    "&a=#{agency}&r=#{route}", resultCallback

  getDeparturesForStop: (agency, stopId, resultCallback) ->
    http.get "http://webservices.nextbus.com/service/publicXMLFeed?command=predictions" +
    "&a=#{agency}&stopId=#{stopId}", resultCallback

root.NextBusSource = NextBusSource
