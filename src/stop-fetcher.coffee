# These classes are responsible for fetching all stops from NextStop (currently limited to
# stops in Northern Californi).

{NextBusFacade} = require './next-bus-facade'
fs = require 'fs'
root = exports ? this

# Removes duplicate stops as determined by the agency and stopId.
removeDuplicateStops = (stops) ->
  alreadyTaken = {}
  filtered = []
  for stop in stops
   duplicateKey = "#{stop.agency}:#{stop.stopId}"
   if !alreadyTaken[duplicateKey]?
     filtered.push stop
     alreadyTaken[duplicateKey] = true
  return filtered

# This class is used to fetch the stops. For performance reasons it limits
class StopFetcher
  # Due to backbone, we autogenerate ids for the stops.
  id: 0
  constructor: (facade = new NextBusFacade()) ->
    @facade = facade

  nextId: ->
    @id++

  # This method retrieves all busstops in the SF area
  getStops: (callback) =>
    @facade.getAgencies (agencies) =>
      @getStopsForAgencies agencies, [], (result) ->
        callback removeDuplicateStops(result)

  # Calls callback with the concatenated list of stops for the agencies. The result parameter
  # is the accumulator, so initially pass [].
  getStopsForAgencies: (agencies, result, callback) =>
    if agencies.length == 0
      callback result
    else
      agency = agencies.shift()
      # We only select routes in Northen California
      if agency.regionTitle == 'California-Northern'
        @facade.getRoutesForAgency agency.tag, (agencyRoutes) =>
          @getStopsForRoutes agency.tag, agencyRoutes, result, (result) =>
            @getStopsForAgencies agencies, result, callback
      else
        @getStopsForAgencies(agencies, result, callback)

  # Given an agency tag, a list of routes, an accumulator and a callback, this method calls
  # callback with the set of stops for these routes.
  getStopsForRoutes: (agency, routes, result, callback) =>
    if routes.length == 0
      callback result
    else
      route = routes.shift()
      @facade.getStopsForRoute agency, route.tag, (newResult) =>
        newResult = @_updateStop(agency, newResult)
        @getStopsForRoutes agency, routes, result.concat(newResult), callback

  # Updates the list of stops by setting agency and id.
  _updateStop: (agency, stopList) =>
    return stopList.map (stop) =>
      stop.agency = agency
      stop.id = @nextId()
      stop

# This class fetches the stops from a locally cached file. If that is not present it fetches the
# stops normally through StopFetcher.
class CachedStopFetcher
  constructor: (filename = 'full-data-set.json') ->
    @filename = filename
    @stopFetcher = new StopFetcher()

  # Calls callback with a list of all Northern Californian stops. If the cache file exists then,
  # the list of stops are extracted from that one, otherwise it is fetched from NextBus' API
  # server.
  getStops: (callback) =>
    fs.exists "#{__dirname}/../#{@filename}", (exists) =>
      if exists
        @getStopsFromCache callback
      else
        @stopFetcher.getStops callback

  getStopsFromCache: (callback) ->
    fs.readFile @filename, (err, data) ->
      if err
        throw new Error "Could not read cache file. #{@filename}. Error was '#{err}'"
      else
        callback removeDuplicateStops(JSON.parse(data))


# The parallel stop fetchers fetches the stops with a single async request for each agency and
# for each route, improving the time to fetch the stops by a factor of more than 15 on NextBus.
class ParallelStopFetcher extends StopFetcher
  getStops: (callback) =>
    @facade.getAgencies (agencies) =>
      callbackReceived = 0
      result = []
      # This function is called when the results from a single agency is processed.
      # If all callback have been received, then we terminate by calling the callback with
      # the result.
      stopsForASingleAgency = (stops) ->
        # TODO: Use an array buffer instead.
        result = result.concat stops
        if ++callbackReceived is agencies.length
          callback removeDuplicateStops(result)

      for agency in agencies
        @getStopsForAgencies([agency], [], stopsForASingleAgency)

  getStopsForRoutes: (agency, routes, result, callback) =>
    callbacksReceived = 0
    stops = result
    stopsForRouteCB = (routeStops) =>
      stops = stops.concat(@_updateStop(agency, routeStops))
      if ++callbacksReceived == routes.length
        callback stops

    for route in routes
      @facade.getStopsForRoute agency, route.tag, stopsForRouteCB

root.StopFetcher = StopFetcher
root.CachedStopFetcher = CachedStopFetcher
root.ParallelStopFetcher = ParallelStopFetcher
