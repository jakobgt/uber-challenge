$ ->
  #############################
  # Models
  #############################
  class Departure extends Backbone.Model
    defaults: () ->
      epochTime: "0"
      seconds: "0"
      minutes: "0"
      routeTitle: ''

  class DepartureList extends Backbone.Collection
    url: '/api/departures/'
    model: Departure

    # We sort by departure.
    comparator: 'epochTime'

  # Represents a stop that is some distance from the current location.
  class Stop extends Backbone.Model
    urlRoot: '/api/stops'
    defaults: () ->
      tag: 'not set'
      title: ''
      lat: 0
      lon: 0
      stopId: -1
      agency: 'foobar'
      distance: -1

  class StopList extends Backbone.Collection
    url: '/api/stoplists/'
    model: Stop
    # We sort by distance.
    comparator: 'distance'

  #############################
  #  Views.
  #############################

  # It is responsible for showing a single departure. If the departure time is less than
  class DepartureView extends Backbone.View
    tagName: 'li'
    template: _.template($('#departure-template').html())

    initialize: =>
      @listenTo @model, 'change', @render
      @listenTo @model, 'remove', @remove

    render: =>
      # If there is no seconds left, we hide it and remove it.
      if (@model.get('epochTime') / 1000) <=  window.util.currentTimeStamp()
        @$el.slideUp =>
          @remove()
      else
        @$el.html @template(@model.toJSON())
      return @

  # This view represents a single stop. The template used should be present in the index.html file.
  class StopView extends Backbone.View
    tagName: 'li'
    template: _.template($('#stop-template').html())

    events:
      "click .clickable":  "toggleDepartures"
      "mouseover .clickable":  "showStopOnMap"
      "mouseout .clickable":  "removeStopOnMap"

    initialize: () ->
      @listenTo @model, 'change', @render
      @listenTo @model, 'remove', @remove
      @departures = new DepartureList()
      @listenTo @departures, 'sync', @addAllDepartures
      @render()

    # This method adds one departure to the list of departures.
    addOneDeparture: (departure) =>
      departureView = new DepartureView model: departure
      @_departuresViewList.append departureView.render().el

    # Adds all departues, loaded in @departures, to the view.
    addAllDepartures: =>
      @resetTimers()
      @departures.each @addOneDeparture

    # This method clears all timers set for syncing with the backend server.
    clearTimers: =>
      if @departuresTimer?
        window.clearInterval(@departuresTimer)
        window.clearInterval(@fetchingTimer)
        @departuresTimer = null
        @fetchingTimer = null

    # resetTimers resets the timers, such that the old timers are deleted and a new timer for
    # fetching departures is set. Furthermore if there are any departures, this method sets a timer
    # to update the view every second to update the shown timers.
    resetTimers: =>
      @clearTimers()
      # We only set to update the view, if there are any departures
      if @departures.length > 0
        @departuresTimer = setInterval(() =>
          @departures.each (departure) -> departure.trigger 'change'
        , 1000)
      @fetchingTimer = setInterval @fetchDepartures, 30000

    # This method simply hides the list item showing that we're fetching stops.
    hideFetchingStops: =>
      @_fetchingStops.slideUp()

    # This method toggles the departures. If the departure list is to be shown an interval is set
    # to refresh the departures with a 30 sec delay in synchronizing with the NextBus server.
    toggleDepartures: =>
      @clearTimers
      request = @fetchDepartures()
      # If the departure list is already visible, then we slideup (the interval is already cleared)
      if @_departuresViewList.is(':visible')
        @_departuresViewList.slideUp()
      else
        @_departuresViewList.slideDown()
        request.always @resetTimers

    # This method refreshes the list of departures.
    # Logic should be on the model, as it contains the references to the server.
    fetchDepartures: =>
      @departures.fetch(
        data: $.param
          id: @model.get 'id'
      ).always =>
        # We hide the fetching one
        @hideFetchingStops()
        # .. and show the no departures one, if there is no
        if @departures.length is 0
          @_noDepartures.slideDown()
        else
          @_noDepartures.hide()

    render: () =>
      @$el.html @template(@model.toJSON())
      # After rendering we access to the elements in the
      @_departuresViewList = $ 'ul.departures', @$el
      @_fetchingStops = $ 'li.fetchingDepartures', @_departuresViewList
      @_noDepartures = $ 'li.noDepartures', @_departuresViewList
      return @

    # Shows a marker on the Google Map for this stop.
    showStopOnMap: =>
      if @marker?
        @marker.setVisible true
      else
        googlePosition = new google.maps.LatLng @model.get('lat'), @model.get('lon')
        @marker =  new google.maps.Marker
          position: googlePosition
          map: googleMap
          icon:
            scale: 4
            path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW

    # Hides if set, the marker for this stop on the map.
    removeStopOnMap: =>
      if @marker?
        @marker.setVisible false


  # The 'global' variable containing the stops.
  stops = new StopList()
  window.stops = stops
  # This is the global variable representing the map.
  googleMap = null
  class UPTAppView extends Backbone.View
    # We bind the view to app already present in the dom.
    el: $('#upt-app')

    events: {
      "keyup #location":  "changedLocation",
      "keyup #radius":  "changedLocation",
      "click #geolocate": "geolocate"
    }

    initialize: () ->
      @stoplist = $ '#stop-list'
      @radiusElm = $ '#radius'
      @locationElm = $ '#location'
      @listenTo stops, 'sync', @addAllStops
      @listenTo stops, 'remove', @removeStop
      @mapCanvas = $ '#map-canvas'
      @initializeMap()
      @updateStops()

    # This method initializes the map, with the starting location given in the input field.
    initializeMap: () =>
      location = @getLocation()
      startPosition = new google.maps.LatLng(location.lat, location.lon);
      mapOptions =
        center: startPosition
        zoom: 13
      # @mapCanvas is the jQuery enhanced version of the mapCanvas, so we access the raw element
      # via [0]
      googleMap = new google.maps.Map @mapCanvas[0], mapOptions

      @googleMarker = new google.maps.Marker
        position: startPosition
        draggable:true
        animation: google.maps.Animation.DROP
        map: googleMap

      infowindow = new google.maps.InfoWindow()
      infowindow.setContent 'Stops around here.'
      infowindow.open googleMap, @googleMarker

      # We need to listen to the Google Maps marker
      google.maps.event.addListener @googleMarker, 'dragend', @markerMoved

    # This method uses the location of the browser and updates the input boxes and calls the
    # changedLocation method.
    geolocate: () =>
      navigator.geolocation.getCurrentPosition (location) =>
        # We utilized the changedLocation method and simply updates the input boxes
        @locationElm.val("#{location.coords.latitude},#{location.coords.longitude}")
        @changedLocation()

    # This method takes the position given in the input boxes and updates the stops and map
    # location.
    changedLocation: =>
      newPosition = @getLocation()
      googlePosition = new google.maps.LatLng newPosition.lat, newPosition.lon
      @googleMarker.setPosition googlePosition
      googleMap.panTo googlePosition
      @updateStops()

    # This method gets the information from the marker, update the input field and
    # updates the stops.
    markerMoved: =>
      newPosition = @googleMarker.getPosition()
      @locationElm.val "#{newPosition.lat()},#{newPosition.lng()}"
      @updateStops()

    render: () =>
      return @

    # This method adds one stop to the StopView list.
    addOneStop: (stop) =>
      # I want to add a stopView.
      view = new StopView model:stop
      @stoplist.append(view.render().el)

    # This method ads all stops.
    addAllStops: =>
      stops.each @addOneStop

    # Removes a stop, by triggering the destroy event on the stop.
    removeStop: (stop) =>
      # We avoid sending the sync request by just destroying the model by triggering the destroy
      # event.
      stop.trigger('destroy', stop, stop.collection);

    # Updates the stops with the current value of location and distance.
    updateStops: (e) =>
      location = @getLocation()
      distance = @getDistance()
      if location?
        stops.fetch
          data: $.param
            lat: location.lat
            lon: location.lon
            dis: distance

    # Returns the location given in the location text field as an object with lat, lon properties.
    getLocation: =>
      textString = @locationElm.val()
      pair = textString.split(',')
      if pair.length == 2
        return {
          lat: pair[0]
          lon: pair[1]
        }
      else
        null

    # Returns the distance given in the input field. If none is given, 0.5 is returned.
    getDistance: =>
      @radiusElm.val() ? '0.5'

  # We initialize the app view.
  app = new UPTAppView()

  # These are simple util functions.
  window.util ?= {}
  window.util.currentTimeStamp = ->
    new Date().getTime() / 1000;
  # Even though the NextBus documentation says that the timestamp is seconds based the output is
  # milli seconds based.
  window.util.minDiff = (ts) ->
      Math.floor(((ts/1000) - window.util.currentTimeStamp()) / 60);
  window.util.secDiff = (ts) ->
      Math.floor(((ts/1000) - window.util.currentTimeStamp()) % 60);
