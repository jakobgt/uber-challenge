# Running this file with node will start up an HTTP server on port 8080, that
# serves the index.html and its related files. Furthermore it serves an API at
# /api
#
# The methods are all GET, the response is JSON and they do the following:
#
# /api/stoplist?lat=12&lon=34&dis=1
# Returns the nearests stops within a distance of `dis` km (in
# this example 1 km) of lat, lon (in this example (12, 34).  The return format is a list with
# each stop. An example of a return value:
# [{"tag":"5696","title":"Market St & Van Ness Ave","lat":"37.7752399","lon":"-122.41918",
#   "stopId":"15696","agency":"sf-muni","id":8051,"distance":0.47}]
# tag, title, lat, lon, stopId, agency are all from NextBux, whereas is is the internal id and
# distance is the distance in km from the given lat and lon in the query parameter.
#
# /api/stops/:id
# Returns the information about the stop with the internal id :id. The format is the same as for
# stoplist, execept that distance is not included.
#
# /api/departures/?id=123
# Returns the departures for the stop for with internal stop id id (here 123).
# The returned format is a list of departures as returned from NextBus (with an added routeTitle):
# [{"epochTime":"1393165633704","seconds":"1582","minutes":"26",
#   "routeTitle":"F-Market & Wharves Inbound to Fisherman's Wharf via Downtown"}]
# The important parts are epochTime that signifies when the bus departs (or that is how I interpret
# it, as it is a bit more involved then that - see README.md). The full object of NextBus are also
# included in the response, but currently is not used in the frontend, and therefore is not further
# discussed here.

http = require 'http'
fs = require 'fs'
express = require 'express'
app = express()
{StopFetcher} = require './src/stop-fetcher'
{CachedStopFetcher} = require './src/stop-fetcher'
{StopsQuerier} = require './src/stops-querier'

fetcher = new CachedStopFetcher()
# We fetch the stops.
fetcher.getStops (result) ->
  console.log 'Loaded stops'
  querier = new StopsQuerier(result)

  # Now we're ready to boot up the HTTP server

  # The root, returns index.html
  app.get '/', (request, response) ->
    respond_with 'client-side/index.html', response

  app.get '/api/stops/:id', (request, response) ->
    stop = querier.find request.params.id
    if stop?
      response.writeHead 200
      response.end JSON.stringify stop
    else
      response.writeHead 404
      response.end 'Record not found'

  # Returns the closest stops.
  app.get '/api/stoplists/', (request, response) ->
    response.writeHead 200
    response.end JSON.stringify(querier.nearestStops(request.query.lat,
      request.query.lon, request.query.dis))

  # Returns the departures for a given stop.
  app.get '/api/departures/', (request, response) ->
    # This is an async call for fetching departures.
    querier.getDepartures request.query.id, (departures) ->
      response.writeHead 200
      departures ?= []
      response.end JSON.stringify(departures)

  # Default route is to serve the file that is requested. Not really safe, as we now serve
  # everything is this directory (including Javascript source code...)
  app.get '/:file(*)', (request, response) ->
    file = request.params.file
    respond_with "client-side/#{file}", response

  # Util function.
  # Convert :id to integer
  app.param 'id', (req, res, next, num, name) ->
    req.params[name] = num = parseInt(num, 10)
    if isNaN(num)
      next(new Error('failed to parseInt '+num))
    else
      next()

  console.log 'Listening on 8080'
  app.listen 8080

# This method is a shorthand for responding with a file.
respond_with = (file, response) ->
  fs.readFile file, 'utf8', (err, data) ->
    if err?
      response.writeHead 404
      response.end "Could not find #{file}."
      console.log "Could not read #{file}: #{err}"
    else
      response.end data
