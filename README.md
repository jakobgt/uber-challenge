
# Links

[Live demo, hosted at NodeJitsu](http://jakobht.nodejitsu.com/)

[Source code](https://bitbucket.org/jakobgt/uber-challenge)

[My resume](http://users-cs.au.dk/~gedefar/uploads/cv.pdf)

[My Github account](https://github.com/jakobgt)

# Run instructions

Run these commands

        npm install
        make build
        node server.js

To run tests

        make test

# Description

This WebApp shows realtime departure information for public transportation in the San Francisco area
from the NextBus source for retrieving the data.
Given a location (e.q., selected via the map), the app shows the nearby public transportation stops.

Clicking on a stop reveals the realtime departures for the the given stop. The departures shown are
syncs with the server every 30 seconds, while the view is updated every second to give the impression
of a counting clock (note though that this second countdown is just for show, as NextBus recommends
that seconds are not shown to users. So on a production site, only the minutes would be shown).

The WebApp is written in CoffeeScript and has three logical components: a backend (
split in a serving backend and a fetching stops backend) and a frontend.

_Serving backend_: Uses NodeJS and Express.js for the HTTP server, that both serves the static (and
  compiled HTML, JS and CSS files) and for the API for serving the frontend. The main class are the
  StopsQuerier, that uses GeoHashing for proximity searches.

_Stop fetching backend_: Retrieves the stops from the NextBus server initially. A cached version
  of the stops are included in `full-data-set.json`. The main classes are `StopFetcher` and
  `ParallelStopFetcher`, where the main difference is that `StopFetcher` retrieves the stops
  linearly and `ParallelStopFetcher` retrieves them in pseudo parallel by utilizing async requests.

_Frontend_: uses Haml for HTML representation, SASS for CSS representation and Backbone.js for model
  and view representation. The main code for the front-end is stored in `client-side/uber-timer.coffee`


## Notes regarding geolocation
According to the [project specification](https://github.com/uber/coding-challenge-tools), the user
should be geolocateable, but as I'm developing in Denmark, will showcase it in Denmark and that
there no stops from NextBus in Denmark, I have chosen to outfactor the geolocate functionality into
a button.

# Assumptions

## General
I assume that the communication with the NextBus server goes without error, as the error handling
is suboptimal. Ideally every async callback should also signal if something went wrong. But right
now the NextBusFacade class simply calls an on error handler.

## NextBus

I have assumed that the departure data from NextBus only list departure times in their epochTime,
but in fact it is more involved (it contains information such as layover and therefor whether the
time is departure time or not).

I have assumed unlimited quota of requests to the NextBus API server, as the departure times are
not cached on the backend server.

I have limited the data to be only around San Francisco (by filtering whether the region is
Northern California)

I assume that different stopids from NextBus means different physical stops, which is a
simplification.

## HTTP server

I have assumed benevolent users, as I'm not enforcing any quota on the number of requests.
Furthermore all files in the client-side library can be downloaded, so some form of cleaning of the
directory would be good (removing CoffeeScript, HAML and SASS files).

# My skills

Before this project, I have not implemented anything in NodeJS, but have worked some with
Coffeescript and JavaScript on the client-side. Only barebone though, so this project is the first
time I use a framework (Backbone.js) for the data representation.

# Test

The backend is fairly well-tested, except for the express server. Due to time constraints the
frontend code is not tested.

# Performance improvements

While developing this web app, I experimented with a number of optimizations, that are highlighted
below.

## Fetching stops
Initially fetching stops from NextBus (in the class StopFetcher) was done linearly (sort of like synchronized,
where the next request for a stop would only be sent after the first had returned).
I changed the algorithm to be asynchronous, with a common callback as the synchronization point
(see ParallelStopFetcher) and that improved the time to fetch stops from ~4.5 min to 16 sec.

Individual results are

A linear approach for 9085 stops
  4 min and 34 sec.

Parallel on agencies (6 agencies) 9085 stops
  2 min and 27 sec. (peak 200 KB/sec, but average around 50KB/sec)

Parallel for fetchings routes by agency: 9085 stops
  25 sec. (Peak: 540 KB/sec)

Parallel for agencies and routes: 9085 stops.
  16 sec. (peak 985 KB/sec)

### Data distribution
The half speedup of being parallel on agencies are explained by the data distribution of
 the agencies, as actransit has a little less than half of the total number of routes (221 in total).

* Number of routes for agency unitrans: 19
* Number of routes for agency emery: 4
* Number of routes for agency ucsf: 16
* Number of routes for agency dumbarton: 2
* Number of routes for agency sf-muni: 81
* Number of routes for agency actransit: 99

## Geohash localized search
Initially the find nearestStops (in StopsQuerier) was a linear scan through the number of stops,
but I changed it to use a GeoHash index to limit the potential candidates.  That reduced the number
of stops to look through by a factor of 7.5 (~ 9000 stops to ~228) for a query in San Francisco with
a distance of 0.5 km. Response time wise, on my Mac, the query time went from from 130ms to 5ms
(waiting time in Chrome), whereas on NodeJitsu it went from ~500ms to ~130ms.

