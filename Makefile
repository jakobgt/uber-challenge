# Very simple makefile for making it easy to deploy to Nodejitsu, as they
# ignore files in .gitignore (hence compiled html, css and JS files)

build: 
	haml --no-escape-attrs client-side/index.haml client-side/index.html
	sass client-side/style.scss client-side/style.css
	coffee -c src/
	coffee -c client-side

# Big hack for a nodejitsu includes html, css and JS files, 
# so we move .gitignore, because jitsu ignores files in .gitignore.
deploy: build
	mv .gitignore dot-gitignore 
	jitsu deploy
	mv dot-gitignore .gitignore 

test-files: build
	coffee -c test/

test: test-files
	mocha -R spec test/**.js

clean:
	rm -f src/*.{js,map}
	rm -f client-side/*.{js,css,html,map}
